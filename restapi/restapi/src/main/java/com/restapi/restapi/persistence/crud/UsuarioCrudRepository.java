package com.restapi.restapi.persistence.crud;


import com.restapi.restapi.persistence.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UsuarioCrudRepository extends CrudRepository<Usuario,Integer> {

	List<Usuario> findByIdTipoDocumentoOrderByEdadAsc(int idTipoDocumento);
	
	Optional<List<Usuario>> findByEdadGreaterThanOrderByEdadAsc(int edad);
	Optional<List<Usuario>> findByEdadLessThanOrderByEdadAsc(int edad);
	Optional<List<Usuario>> findByEdadGreaterThanEqualOrderByEdadAsc(int edad);
	Optional<List<Usuario>> findByEdadLessThanEqualOrderByEdadAsc(int edad);
}
