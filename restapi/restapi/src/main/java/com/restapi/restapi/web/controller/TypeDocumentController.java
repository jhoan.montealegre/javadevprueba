package com.restapi.restapi.web.controller;



import com.restapi.restapi.domain.TypeDocument;
import com.restapi.restapi.domain.service.TypeDocumentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/typeDocument")
public class TypeDocumentController {
	@Autowired
	private TypeDocumentService typeDocumentService;
	
	@GetMapping("/all")
	@ApiOperation(value = "Get all type documents")
	@ApiResponse(code = 200, message = "OK")
	public ResponseEntity<List<TypeDocument>> getAll(){
		return new ResponseEntity<>(typeDocumentService.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{typeDocumentId}")
	@ApiOperation(value = "Search a type document with an ID")
	@ApiResponses({
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Type document not found"),
	})
	public ResponseEntity<TypeDocument> getTypeDocumentById(@PathVariable("typeDocumentId") int typeDocumentId){
		return typeDocumentService.getTypeDocument(typeDocumentId)
				.map(td -> new ResponseEntity<>(td,HttpStatus.OK)).
				orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete type type document by id")
	@ApiResponse(code = 200, message = "OK")
	public ResponseEntity deleteTypeDocumentById(@PathVariable("id") int typeDocumentId) {
		if( typeDocumentService.deleteTypeDocumentById(typeDocumentId)) {
			return new ResponseEntity(HttpStatus.OK);
		}else {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
	}

	@PostMapping("/save")
	@ApiOperation(value = "save a type document")
	@ApiResponse(code = 201, message = "CREATED")
	public ResponseEntity<TypeDocument> saveCustomer(@RequestBody TypeDocument typeDocument) {
		return new ResponseEntity<>(typeDocumentService.saveTypeDocument(typeDocument),HttpStatus.CREATED);
	}
	
	
}
