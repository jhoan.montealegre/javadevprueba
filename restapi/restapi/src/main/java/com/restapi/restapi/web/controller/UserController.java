package com.restapi.restapi.web.controller;


import com.restapi.restapi.domain.User;
import com.restapi.restapi.domain.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	@ApiOperation(value = "Get all users")
	@ApiResponse(code = 200, message = "OK")
	public ResponseEntity<List<User>> getAll(){
		return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/{userId}")
	@ApiOperation(value = "Search a user with an ID")
	@ApiResponses({
			@ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 404, message = "Usernot found"),
	})
	public ResponseEntity<User> getUser(@PathVariable("userId") int userId){
		return new ResponseEntity<>(userService.getUser(userId),HttpStatus.OK);
				//orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@GetMapping("/typeDocument/{idTypeDocument}")
	@ApiOperation(value = "Search all users with an speciiic type document")
	@ApiResponses({
			@ApiResponse(code = 200, message = "OK")
	})
	public ResponseEntity<List<User>> getByTypeDocument(@PathVariable("idTypeDocument") int idTypeDocument){
		return userService.getByTypeDocument(idTypeDocument)
				.map(cust -> new ResponseEntity<>(cust, HttpStatus.OK)).
				orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping("/save")
	@ApiOperation(value = "save a user")
	@ApiResponse(code = 201, message = "CREATED")
	public ResponseEntity<User> saveUser(@RequestBody User user) throws IOException {

		return new ResponseEntity<>(userService.saveUser(user),HttpStatus.CREATED);

	}
	
	@GetMapping("/lessThanEqual/{age}")
	public Optional<List<User>> getUserLessThanEqual(@PathVariable("age") int age){
		return userService.getUserLessThanEqual(age);
	}
	
	@GetMapping("/lessThan/{age}")
	public Optional<List<User>> getUserLessThan(@PathVariable("age") int age){
		return userService.getUserLessThan(age);
	}
	
	@GetMapping("/greaterThanEqual/{age}")
	public Optional<List<User>> getUserGreaterThanEqual(@PathVariable("age") int age){
		return userService.getUserGreaterThanEqual(age);
	}
	
	@GetMapping("/greaterThan/{age}")
	public Optional<List<User>> getUserGreaterThan(@PathVariable("age") int age){
		return userService.getUserGreaterThan(age);
	}
	
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete user by id")
	@ApiResponse(code = 200, message = "OK")
	public ResponseEntity deleteUserById(@PathVariable("id") int userId) {
		//if(customerService.deleteCustomerById(customerId)) {
		userService.deleteUserById(userId);
			return new ResponseEntity( HttpStatus.OK);
		//}else {
		//	return new ResponseEntity(HttpStatus.NOT_FOUND);
	//}
		
	}
}
