package com.restapi.restapi.persistence.mapper;


import com.restapi.restapi.domain.User;
import com.restapi.restapi.persistence.entity.Usuario;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TypeDocumentMapper.class})
public interface UserMapper {

	@Mappings({
		@Mapping(source="id",target="userId"),
		@Mapping(source="nombre",target="name"),
		@Mapping(source="apellidos",target="lastName"),
		@Mapping(source="edad",target="age"),
		@Mapping(source="ciudadNacimiento",target="cityOfTheBirth"),
		@Mapping(source="idTipoDocumento",target="typeDocumentId"),
		@Mapping(source="tipoDocumento",target="typeDocument"),
		@Mapping(source="numeroIdentificacion",target="identificationNumber")
	})
	User toUser(Usuario usuario);
	
	List<User> toUsers(List<Usuario> usuarios);
	
	@InheritInverseConfiguration
    Usuario toUsuario(User user);
}
