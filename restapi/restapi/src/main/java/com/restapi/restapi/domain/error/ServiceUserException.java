package com.restapi.restapi.domain.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ServiceUserException extends RuntimeException{

    private HttpStatus httpStatus;

    public ServiceUserException(HttpStatus httpStatus, String errorMessage){
        super(errorMessage);
        this.httpStatus = httpStatus;
    }
}
