package com.restapi.restapi.domain.repository;



import com.restapi.restapi.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

	List<User> getAll();
	Optional<List<User>> getByTypeDocument(int idTypeDocument);
	User getUserById(int id);
	User saveCustomer(User user);
	Optional<List<User>> getUserLessThanEqual(int age);
	Optional<List<User>> getUserLessThan(int age);
	Optional<List<User>> getUserGreaterThanEqual(int age);
	Optional<List<User>> getUserGreaterThan(int age);
	void deleteUserById(int customerId);
}
