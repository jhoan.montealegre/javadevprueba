package com.restapi.restapi.persistence;


import com.restapi.restapi.domain.User;
import com.restapi.restapi.domain.repository.UserRepository;
import com.restapi.restapi.persistence.crud.UsuarioCrudRepository;
import com.restapi.restapi.persistence.entity.Usuario;
import com.restapi.restapi.persistence.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class UsuarioRepository implements UserRepository {
	
	@Autowired
	private UsuarioCrudRepository usuarioCrudRepository;
	
	@Autowired
	private UserMapper mapper;
	
	@Override
	public List<User> getAll() {
		List<Usuario> usuarios = (List<Usuario>) usuarioCrudRepository.findAll();
		return mapper.toUsers(usuarios);
	}

	@Override
	public Optional<List<User>> getByTypeDocument(int idTypeDocument) {
		List<Usuario> usuarios = usuarioCrudRepository.findByIdTipoDocumentoOrderByEdadAsc(idTypeDocument);
		return Optional.of(mapper.toUsers(usuarios));
	}

	@Override
	public User getUserById(int id) {
		User customer1 = mapper.toUser(usuarioCrudRepository.findById(id).orElse(null));
		return customer1;
	}

	@Override
	public User saveCustomer(User customer) {
		Usuario usuario = mapper.toUsuario(customer);
		return mapper.toUser(usuarioCrudRepository.save(usuario));
	}

	@Override
	public Optional<List<User>> getUserLessThanEqual(int age) {
		Optional<List<Usuario>> usuarios = usuarioCrudRepository.findByEdadLessThanEqualOrderByEdadAsc(age);
		return usuarios.map(c -> mapper.toUsers(c));
	}

	@Override
	public Optional<List<User>> getUserLessThan(int age) {
		Optional<List<Usuario>> usuarios = usuarioCrudRepository.findByEdadLessThanOrderByEdadAsc(age);
		return usuarios.map(c -> mapper.toUsers(c));
	}

	@Override
	public Optional<List<User>> getUserGreaterThanEqual(int age) {
		Optional<List<Usuario>> usuarios = usuarioCrudRepository.findByEdadGreaterThanEqualOrderByEdadAsc(age);
		return usuarios.map(c -> mapper.toUsers(c));
	}

	@Override
	public Optional<List<User>> getUserGreaterThan(int age) {
		Optional<List<Usuario>> usuarios = usuarioCrudRepository.findByEdadGreaterThanOrderByEdadAsc(age);
		return usuarios.map(c -> mapper.toUsers(c));
	}

	@Override
	public void deleteUserById(int userId) {
		usuarioCrudRepository.deleteById(userId);
	}

	
	
}
