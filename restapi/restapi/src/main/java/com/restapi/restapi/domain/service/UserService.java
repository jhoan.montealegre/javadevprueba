package com.restapi.restapi.domain.service;


import com.restapi.restapi.domain.User;

import com.restapi.restapi.domain.repository.UserRepository;

import com.restapi.restapi.domain.error.ServiceUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;



	public List<User> getAll(){
		List<User> users = userRepository.getAll();
		if(users.isEmpty()){
			throw new ServiceUserException(HttpStatus.NOT_FOUND,"No existen clientes en la base de datos");
		}
		return users;
	}
	
	public User getUser(int userId){
		User user = userRepository.getUserById(userId);
		if(user == null){
			throw new ServiceUserException(HttpStatus.NOT_FOUND,"No existen el cliente");
		}

		return user;
	}
	
	public Optional<List<User>> getByTypeDocument(int idTypeDocument){
		return userRepository.getByTypeDocument(idTypeDocument);
	}
	
	public User saveUser(User user) throws IOException {
		User user1 = userRepository.getUserById(user.getUserId());
		if(user1 != null){
			throw new ServiceUserException(HttpStatus.BAD_REQUEST," el cliente ya existe");
		}
		user1 = userRepository.saveCustomer(user);
		return  user1;

	}
	
	public Optional<List<User>> getUserLessThanEqual(int age){
		return userRepository.getUserGreaterThanEqual(age);
	}
	public Optional<List<User>> getUserLessThan(int age){
		return userRepository.getUserLessThan(age);
	}
	public Optional<List<User>> getUserGreaterThanEqual(int age){
		return userRepository.getUserGreaterThanEqual(age);
	}
	public Optional<List<User>> getUserGreaterThan(int age){
		return userRepository.getUserGreaterThan(age);
	}
	public void deleteUserById(int userId) {
		User user = userRepository.getUserById(userId);
		if(user == null) {
			throw new ServiceUserException(HttpStatus.BAD_REQUEST, "El usuario no existe");
		}

		userRepository.deleteUserById(userId);
		//return getCustomer(customerId).map(cust -> {


			//return true;
		//}).orElse(false);

	}

}
